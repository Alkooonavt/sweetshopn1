﻿using System.Collections.Generic;
using Domain.Model;

namespace WebApp.Models.Order
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public Domain.Model.Customer Customer { get; set; }

        public decimal TotalPrice { get; set; }

        public List<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();

    }
}