﻿using System.Collections.Generic;

namespace WebApp.Models.Customer
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public List<Domain.Model.Order> Orders { get; set; } = new List<Domain.Model.Order>();
    }
}