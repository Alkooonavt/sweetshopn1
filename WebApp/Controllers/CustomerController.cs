﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Domain.Model;
using Infrastructure.Database.Interfaces;
using WebApp.Models.Customer;
using WebApp.Models.Order;

namespace WebApp.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public CustomerController(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            var customers = _uow.Customers.GetAll();
            var customersViewModel = _mapper.Map<IList<CustomerViewModel>>(customers);
            return View(customersViewModel);
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            var customerViewModel = _mapper.Map<CustomerViewModel>(_uow.Customers.Get(id));
            return View(customerViewModel);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(CustomerViewModel customerViewModel)
        {
            if (!ModelState.IsValid) return View();
            var customer = _mapper.Map<Customer>(customerViewModel);
            _uow.Customers.Create(customer);
            _uow.Save();
            return RedirectToAction("Index");
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null || id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var customer = _uow.Customers.Get((int)id);
            if (customer == null) return HttpNotFound();
            var customerEdit = _mapper.Map<CustomerViewModel>(customer);
            return View(customerEdit);
        }

        [HttpPost]
        public ActionResult Edit(CustomerViewModel customerViewModel)
        {
            if (!ModelState.IsValid) return View();
            var customer = _mapper.Map<Customer>(customerViewModel);
            _uow.Customers.Edit(customer);
            _uow.Save();
            return RedirectToAction("Index");
        }

        // GET: Customer/Delete/5

        public ActionResult Delete(int? id)
        {
            if (id == null || id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var customer = _uow.Customers.Get((int)id);
            if (customer == null) return HttpNotFound();
            var customerEdit = _mapper.Map<CustomerViewModel>(customer);
            return View(customerEdit);
        }

        [HttpPost]
        public ActionResult Delete(CustomerViewModel customerViewModel)
        {
            if (!ModelState.IsValid) return View();
            var customer = _mapper.Map<Customer>(customerViewModel);
            _uow.Customers.Delete(customer);
            return RedirectToAction("Index");
        }
    }
}
