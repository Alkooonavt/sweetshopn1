﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Domain.Model;
using Infrastructure.Database.EFImplementations;
using Infrastructure.Database.Interfaces;
using WebApp.Models.Order;
using WebGrease.Css.Extensions;

namespace WebApp.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public OrdersController(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        public ActionResult Index()
        {
            var model = _uow.Orders.GetAll();
            var modelList = _mapper.Map<IList<Order>, IList<OrderViewModel>>(model);
            return View(modelList);
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var student = _uow.Orders.Get((int)id);
            var studentViewModel = _mapper.Map<OrderViewModel>(student);
            if (student != null) return View(studentViewModel);
            return HttpNotFound();
        }

        public ActionResult Create() => View();
        [HttpPost]
        public ActionResult Create(OrderViewModel orderViewModel)
        {
            if (!ModelState.IsValid) return View();
            var order = _mapper.Map<Order>(orderViewModel);
            _uow.Orders.Create(order);
            _uow.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null || id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var order = _uow.Orders.Get((int)id);
            if (order == null) return HttpNotFound();
            var editOrder = _mapper.Map<OrderViewModel>(order);
            return View(editOrder);
        }

        [HttpPost]
        public ActionResult Edit(OrderViewModel orderViewModel)
        {
            if (!ModelState.IsValid) return View();
            var order = _mapper.Map<Order>(orderViewModel);
            _uow.Orders.Edit(order);
            _uow.Save();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null || id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var order = _uow.Orders.Get((int)id);
            if (order == null) return HttpNotFound();
            var editOrder = _mapper.Map<OrderViewModel>(order);
            return View(editOrder);
        }

        [HttpPost]
        public ActionResult Delete(OrderViewModel orderViewModel)
        {
            if (!ModelState.IsValid) return View();
            var order = _mapper.Map<Order>(orderViewModel);
            _uow.Orders.Delete(order);
            return RedirectToAction("Index");
        }
    }
}