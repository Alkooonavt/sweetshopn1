﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Model;
using Infrastructure.Database.Interfaces;
using Infrastructure.EntityFramework;

namespace Infrastructure.Database.EFImplementations
{
    public class CustomerRepository : IRepository<Customer>
    {
        private readonly IUnitOfWork _uow;

        public CustomerRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Customer Get(int id)
        {
            return _uow.Customers.Get(id);
        }

        public IList<Customer> GetAll()
        {
            return _uow.Customers.GetAll();
        }

        public Customer Create(Customer entity)
        {
            return _uow.Customers.Create(entity);
        }

        public Customer Edit(Customer entity)
        {
            return _uow.Customers.Edit(entity);
        }

        public Customer Delete(Customer entity)
        {
            return _uow.Customers.Delete(entity);
        }
    }
}