﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Model;
using Infrastructure.Database.Interfaces;
using Infrastructure.EntityFramework;

namespace Infrastructure.Database.EFImplementations
{
    public class OrdersRepository : IRepository<Order>
    {
        private readonly IUnitOfWork _uow;
        public OrdersRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Order Get(int id)
        {
            return _uow.Orders.Get(id);
        }

        public IList<Order> GetAll()
        {
            return _uow.Orders.GetAll();
        }

        public Order Create(Order entity)
        {
            return _uow.Orders.Create(entity);
        }

        public Order Edit(Order entity)
        {
            return _uow.Orders.Edit(entity);
        }

        public Order Delete(Order entity)
        {
            return _uow.Orders.Delete(entity);
        }
    }
}