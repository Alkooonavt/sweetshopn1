﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Model;
using Infrastructure.Database.Interfaces;
using Infrastructure.EntityFramework;

namespace Infrastructure.Database.EFImplementations
{
    public class ProductsRepository : IRepository<Product>
    {
        private readonly IUnitOfWork _uow;

        public ProductsRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Product Get(int id)
        {
            return _uow.Products.Get(id);
        }

        public IList<Product> GetAll()
        {
            return _uow.Products.GetAll();
        }

        public Product Create(Product entity)
        {
            return _uow.Products.Create(entity);
        }

        public Product Edit(Product entity)
        {
            return _uow.Products.Edit(entity);
        }

        public Product Delete(Product entity)
        {
            return _uow.Products.Delete(entity);
        }
    }
}
