﻿using System.Collections.Generic;
using Domain.Model;
using Infrastructure.Database.Interfaces;
using Infrastructure.EntityFramework;

namespace Infrastructure.Database.EFImplementations
{
    public class OrderDetailsRepository : IRepository<OrderDetail>
    {
        private readonly IUnitOfWork _uow;

        public OrderDetailsRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public OrderDetail Get(int id)
        {
            return _uow.OrderDetails.Get(id);
        }

        public IList<OrderDetail> GetAll()
        {
            return _uow.OrderDetails.GetAll();
        }

        public OrderDetail Create(OrderDetail entity)
        {
            return _uow.OrderDetails.Create(entity);
        }

        public OrderDetail Edit(OrderDetail entity)
        {
            return _uow.OrderDetails.Edit(entity);
        }

        public OrderDetail Delete(OrderDetail entity)
        {
            return _uow.OrderDetails.Delete(entity);
        }
    }
}